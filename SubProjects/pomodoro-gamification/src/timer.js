// values should be in seconds
const workLength = 20;
const breakLength = 5;
const workUpdateInterval = 1*1000; // 1 second

var status = "work";
var elapsed = 0;

function startTimer() {
  return setInterval(updateTime, workUpdateInterval);
}

// add the elapsed time to our counter
function updateTime() {
  elapsed += workUpdateInterval;
  // console.log(`status=${status}; elapsedTime=${elapsed}`)

  checkAndToggleStatus();
  updateRemainingTime();
}

// checks the current status and updates it according to the elapsed
// time
function checkAndToggleStatus() {
  if (elapsed >= breakLength*1000 && status == "break") {
    status = "work";
    elapsed = 0;
  } else if (elapsed >= workLength*1000 && status == "work") {
    status = "break"
    elapsed = 0;
  }

  // update UI with the status
  let currentStatus = document.getElementById("status");
  currentStatus.innerHTML = status;
}

// Return remaining time for the current task. Default is formatted to
// text, if inMilliseconds is true, then return time in milliseconds
function getRemainingTime(inMilliseconds) {
  if (inMilliseconds) return elapsed;

  let minutes = Math.floor(elapsed / 1000 / 60);
  let seconds = (elapsed - minutes*60*1000) / 1000;

  return `${minutes}:${seconds}`;
}

// Update the UI with the remaining time
function updateRemainingTime() {
  let timer = document.getElementById("timer");
  timer.innerHTML = getRemainingTime(false);
}
